<?php

use Illuminate\Support\Facades\Route;
use App\Models\Customer;
use App\Models\Product;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/customer',function(){
    $customers =Customer::all();
    echo "<pre>";
    print_r($customers->toArray());
});
Route::get('/product',function(){
    $product=Product::all();
    echo "<pre>";
    print_r($product->toArray());
});