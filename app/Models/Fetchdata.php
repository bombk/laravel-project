<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fetchdata extends Model
{
    use HasFactory;
    protected $table='amp';
    protected $primarykey='Id';
}
