<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/data/{name?}',function($name=null){
    $data=compact('name');
    return view('home') -> with ($data);
});
Route::get('/',function($name=null){
   return view('home');
});
Route::get('/about',function($name=null){
    return view('about');
 });

 Route::get('/course',function($name=null){
    return view('course');
 });
