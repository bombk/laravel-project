<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<nav>
        <a href="{{url('/')}}">Home</a>
        <a href="{{url('/')}}/register">Register</a>
        <a href="{{url('/customer')}}/">Customer</a>
    </nav>
    <form action="{{$url}}" Method="POST">
        @csrf
    <h1>{{$title}}</h1>

    <x-form type="text" name="name" placeholder="Enter your name" label="Enter your name" />
    <x-form type="email" name="email" placeholder="Enter your email" label="Enter your email" />
    <x-form type="password" name="password" placeholder="Enter your password" label="Enter your password"/>
    <x-form type="password" name="confirm_password" placeholder="Confirm Password" label="Confirm Passwrod"/>
    <x-form type="text" name="country" placeholder="Enter country" label="Plase enter country"/>
    <x-form type="text" name="state" placeholder="enter state" label="enter state"/>
    <x-form type="text" name="address" placeholder="Enter address" label="Enter address"/>
    <x-form type="text" name="gender" placeholder="enter gender" label="enter gender"/>
    <x-form type="text" name="dob" placeholder="enter dob" label="enter dob"/>
    <x-form type="text" name="status" placeholder="enter status" label="enter status"/>
    <button>Submit</button>
    </form>
</body>
</html>