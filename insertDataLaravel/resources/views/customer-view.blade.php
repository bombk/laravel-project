<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Customer view page</h1>

    <nav>
        <a href="{{url('/')}}">Home</a>
        <a href="{{url('/')}}/register">Register</a>
        <a href="{{url('/customer')}}/">Customer</a>
    </nav>
    <a href="{{route('customer-create')}}">

        <button>Add</button>
    </a>


    <div class="continer">

    <form action="" >
        <input type="search" name="search" id="" placeholder="search by name" value={{$search}}>
        <button>Search</button>
        <a href="{{url('/customer')}}">
            <button type="button">Reset</button>
        </a>


    </form>


    <table border=1>
        <!-- <pre>
            {{print_r($customer)}}
        </pre> -->

        <thead>
            <tr>
                <th>name</th>
                <th>email</th>
                <th>country</th>
                <th>state</th>
                <th>address</th>
                <th>gender</th>
                <th>dob</th>
                <th>status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($customer as $cust)
            <tr>
                <td>{{$cust->name}}</td>
                <td>{{$cust->email}}</td>
                <td>{{$cust->country}}</td>
                <td>{{$cust->state}}</td>
                <td>{{$cust->address}}</td>
                <td>{{$cust->gender}}</td>
                <td>{{$cust->dob}}</td>
                <td>
                    @if ($cust->status=="1")
                    <a href="">
                    Active
                    </a>
                    @else
                    <a href="">
                    Inactive
                    </a>
                    @endif
                </td>
                <td>
                    <!-- <a href="{{url('/customer/delete')}}/{{$cust->id}}"> -->
                    <a href="{{route('customer-delete',['id'=>$cust->id])}}">
                    <button>Delete</button>
                    </a>
                    <a href="{{route('customer-edit',['id'=>$cust->id])}}">
                    <button>Edit</button>
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>


    </div>
</body>
</html>