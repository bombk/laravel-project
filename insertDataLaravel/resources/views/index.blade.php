<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Database show</h2>
    <h1>Session data = {{session()->get('user_name')}}</h1>
    @if (session()->has('user_name'))
    {{session()->get('user_name')}}
    @else
    Guest
    @endif
    <nav>
        <a href="{{url('/')}}">Home</a>
        <a href="{{url('/')}}/register">Register</a>
        <a href="{{url('/customer')}}/">Customer</a>
    </nav>
</body>
</html>