<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomerController;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/register',[CustomerController::class,'index']);
Route::post('/register',[CustomerController::class,'store'])->name('customer-create');
Route::get('/customer',[CustomerController::class,'view']);
Route::get('/customer/delete/{id}',[CustomerController::class,'delete'])->name('customer-delete');
Route::get('/customer/edit/{id}',[CustomerController::class,'edit'])->name('customer-edit');
Route::post('/customer/update/{id}',[CustomerController::class,'update'])->name('customer-update');
Route::get('get-all-session',function(){
    $session =session()->all();
    echo "<pre>";
    print_r($session);
    
});

Route::get('set-session',function(Request $request){

    $request->session()->put('user_name','bombk');
    $request->session()->put('user_id','123');
    $request->session()->flash('status','success');
    return redirect('get-all-session');

});

Route::get('destroy-session',function(){
    session()->forget('user_name');
    session()->forget('user_id');
    return redirect('get-all-session');
});