<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;

class CustomerController extends Controller
{
    //
    public function index(){
        $url=url('/register');
        $title="Customer registration!";
        $data=compact('url','title');
        return view('form')->with($data);
    }
    public function store(Request $request){

        // echo "<pre>";
        // print_r($request->all());
        //Insert query;

        $customer = new Customer;
        $customer->name=$request['name'];
        $customer->email=$request['email'];
        $customer->password=$request['password'];
        $customer->confirm_password=$request['confirm_password'];
        $customer->country=$request['country'];
        $customer->state=$request['state'];
        $customer->address=$request['address'];
        $customer->gender=$request['gender'];
        $customer->dob=$request['dob'];
        $customer->status=$request['status'];
        $customer->save();

        return redirect('/customer');

    }
    public function view(Request $request){

        $search=$request['search'] ?? "";
        if($search !=""){

            // $customer=Customer::where('name','=',$search)->get(); //search correct data
            // $customer=Customer::where('name','LIKE',"%$search")->get();
            $customer=Customer::where('name','LIKE',"%$search")->orwhere('email','LIKE',"%$search")->get();


        }
        else{

            $customer = Customer::all();

        }
        // echo "<pre>";
        // print_r($customer->toArray());
        // die;
        // echo "</pre>";
        $data=compact('customer','search');

        return view('customer-view')->with($data);
    }

    public function delete($id){
       // echo $id;
    //    $customer=Customer::find($id)->delete(); //direct delete method

       $customer=Customer::find($id);
       if(!is_null($customer)){
        $customer->delete();
       }
    //    print($customer); //retrun 1 if success
    //    echo "<pre>";
    //    print_r($customer->toArray());

        return redirect('customer');

    }
    public function edit($id){

        // echo $id;
        $customer=Customer::find($id);

        if(is_null($customer)){
            return redirect('customer');
        }
        else{
            $url=url('/customer/update')."/".$id;
            $title="Update customer!";
            $data=compact('customer','url','title');
            return view('form')->with($data);
        }

    }
    public function update($id ,Request $request){
        $customer=Customer::find($id);
        $customer->name=$request['name'];
        $customer->email=$request['email'];
        $customer->country=$request['country'];
        $customer->state=$request['state'];
        $customer->address=$request['address'];
        $customer->gender=$request['gender'];
        $customer->dob=$request['dob'];
        $customer->status=$request['status'];
        $customer->save();
        return redirect('/customer');


    }


}
