<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Form extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $label;
    public $name;
    public $type;
    public $placeholder;
    public $value;
    public function __construct($label,$name,$type,$placeholder,$value=null)
    {
        //
        $this->label=$label;
        $this->name=$name;
        $this->type=$type;
        $this->placeholder=$placeholder;
        $this->value=$value;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form');
    }
}
