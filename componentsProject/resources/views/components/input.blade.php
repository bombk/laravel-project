<div>
    <!-- He who is contented is rich. - Laozi -->
    <label for="">{{$label}}</label>
    <input type="{{$type}}" name="{{$name}}" placeholder="{{$placeholder}}">
    <span>
        {{$demo}}
    </span>
</div>