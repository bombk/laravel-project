<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Input extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

     public $label;
     public $type;
     public $name;
     Public $placeholder;
     public $demo;


    public function __construct($label,$type,$name,$placeholder,$demo=0)
    {
        //
        $this->label=$label;
        $this->type=$type;
        $this->name=$name;
        $this->placeholder=$placeholder;
        $this->demo=$demo;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.input');
    }
}
